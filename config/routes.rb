Rails.application.routes.draw do
  devise_for :admins
  devise_for :users,controllers:{
      session: 'users/sessions'
      }
  resources :zombies do
    resources :brains
      devise_scope :admin do
          get "admins/users/list", to: "admins/sessions#list", as: "users_list"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
    root to: 'zombies#index'
end